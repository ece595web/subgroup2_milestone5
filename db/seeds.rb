# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
   User.create(email: 'sriram@admin.com', password: 'ram12345', password_confirmation: 'ram12345', role_type: 'Admin')
  
   colleges = College.create([{ name: 'RIT' }, { name: 'CBIT' }, { name: 'YnCollege' }])
   departments = Department.create([{ name: 'Physics',college_id: '1' }, { name: 'BPO',college_id: '2' }, { name: 'Automation',college_id: '3' }, { name: 'Testing',college_id: '1' }, { name: 'IT',college_id: '2' }, { name: 'CSC',college_id: '3' }])
