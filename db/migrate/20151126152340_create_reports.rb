class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :college_id
      t.integer :department_id
      t.integer :course_id
      t.integer :user_id
      t.datetime :assessment_start_period
      t.datetime :assessment_end_period
      t.datetime :submitted_date
      t.string :status
      t.text :student_learning_utcomes
      t.string :unm_learning_goals
      t.string :unm_strategic_plan
      t.string :assessment_measures
      t.string :performance_benchmark
      t.string :data_results
      t.string :data_analysis
      t.text :recommendations
      t.text :met_details

      t.timestamps null: false
    end
  end
end
