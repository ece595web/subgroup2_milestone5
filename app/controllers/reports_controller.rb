class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy, :submit_report]
  before_action :authenticate_user!
  before_filter :require_permission, only: [:new,:create]

  # GET /reports
  # GET /reports.json
  def index
    if current_user.role_type == 'Advisor'
      @reports = Report.where('user_id=?',current_user.id).order('updated_at DESC')
    elsif current_user.role_type == 'Director'
      @reports = Report.where('college_id =? AND department_id=? AND status=?',current_user.college_id,current_user.department_id,'Submitted').order('updated_at DESC')
    else
      @reports = Report.all.order('updated_at DESC')
    end
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
   @departments = @report.college_id.present? ? Department.where('college_id=?',@report.college_id) : []
   @courses = @report.department_id.present? ? Course.where('department_id=?',@report.department_id) : []
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params)
    @report.status = params[:report].any?{|k,v| v.empty?} ? 'Incomplete' : 'Completed'
    @report.user_id = current_user.id
    
    respond_to do |format|
      if @report.save
        format.html { redirect_to reports_url, notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    respond_to do |format|
     @report.status = params[:report].any?{|k,v| v.empty?} ? 'Incomplete' : 'Completed'
      if @report.update(report_params)
        format.html { redirect_to reports_url, notice: 'Report was successfully saved.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_departments_list
  @departments = Department.where('college_id=?',params[:college_id])
  @department_id = @departments.first.id
  @courses = Course.where('department_id=?',@department_id)
  end

  def get_courses_list
    @courses = Course.where('department_id=?',params[:department_id])
  end

  def submit_report
   @report.update_attributes(:status=>'Submitted')
   respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully Submitted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:college_id, :department_id, :course_id, :assessment_start_period, :assessment_end_period, :submitted_date, :status, :student_learning_utcomes, :unm_learning_goals, :unm_strategic_plan, :assessment_measures, :performance_benchmark, :data_results, :data_analysis, :recommendations, :user_id, :met_details)
    end



def require_permission
  if user_role != 'Advisor'
    respond_to do |format|
      format.html { redirect_to reports_url, notice: "You can't create reports." }
    end
  else
   	return true
  end
end
end
