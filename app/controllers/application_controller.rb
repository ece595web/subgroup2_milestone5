class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def user_role
    User.where('id=?',current_user.id).first.role_type if current_user
  end
  helper_method :user_role
end
